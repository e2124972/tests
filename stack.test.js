const st = require('./stack.js');
//test si push ajoute bien la valeurs avec peek.
test('Test si push ajoute bien la valeurs avec peek.', () => {
	//arrange
  const t1 = new st.Stack();
  //act
  t1.push(1);
  const val = t1.peek();
  //asert
  expect(val).toEqual(1);
});
//Count si une pile est bien vide a sa création. (avec count)
test('Test si la pile est vide à sa création.', () => {
	//arrange
  const t1 = new st.Stack();
  //act
  const count = t1.count();
  //asert
  expect(count).toEqual(0);
});
//test si la pile est vide à sa création. (avec empty)
test('Test si la pile est vide à sa création.', () => {
  //arrange
  const t1 = new st.Stack();
  //act
  const empty = t1.isEmpty();
  //asert
  expect(empty).toBeTruthy();
});
//apres push test si empty est false.
test('Apres push test si empty est false.', () => {
  //arrange
	const t1 = new st.Stack();
  //act
  t1.push()
  const empty = t1.isEmpty();
  //asert
  expect(empty).toBeFalsy();
});
//Test si count est plus haut apres un push
test('Test si count est plus haut apres un push.', () => {
  //arrange
	const t1 = new st.Stack();
  //act
  const val1 = t1.count();
  t1.push(1)
  const val2 = t1.count();
  //asert
  expect(val2).toBeGreaterThan(val1);
});
//Test si pop enleve bien une valeur (avec count)
test('Test si pop enleve bien une valeur (avec count).', () => {
  //arrange
	const t1 = new st.Stack();
  //act
  t1.push(1)
  const val1 = t1.count();
  t1.pop()
  const val2 = t1.count();
  //asert
  expect(val1).toBeGreaterThan(val2);
});
//Test si peek ne change pas le count.
test('Test si peek ne change pas le count.', () => {
  //arrange
	const t1 = new st.Stack();
  //act
  const val1 = t1.count();
  t1.peek()
  const val2 = t1.count();
  //asert
  expect(val1).toEqual(val2);
});
//Test si pop d'une pile vide retourne null
test('Test si pop d-une pile vide retourne null.', () => {
  //arrange
	const t1 = new st.Stack();
  //act
  const val1 = t1.pop();
  //asert
  expect(val1).toBeNull();
});
//Test si peek d'une pile vide retourne null
test('Test si peek d-une pile vide retourne null.', () => {
  //arrange
	const t1 = new st.Stack();
  //act
  const val1 = t1.peek();
  //asert
  expect(val1).toBeNull();
});
//test si pop retourne le derniere valeur ajouter par push
test('test si pop retourne le derniere valeur ajouter par push.', () => {
	//arrange
  const t1 = new st.Stack();
  //act
  t1.push(1);
  const val = t1.peek();
  //asert
  expect(val).toEqual(1);
});
//test si peek retourne le derniere valeur ajouter par push
test('test si peek retourne le derniere valeur ajouter par push.', () => {
	//arrange
  const t1 = new st.Stack();
  //act
  t1.push(1);
  const val = t1.peek();
  //asert
  expect(val).toEqual(1);
});
//test si les valeur sont empiler correctement
test('test si les valeur sont empiler correctement.', () => {
	//arrange
  const t1 = new st.Stack();
  //act
  t1.push(1);
  t1.push(121);
  t1.push(10234);
  const val = t1.pop();
  const val2 = t1.pop();
  const val3 = t1.pop();
  //asert
  expect(val).toEqual(10234);
  expect(val2).toEqual(121);
  expect(val3).toEqual(1);
});